"""
Task 3-1
--------

- create an numpy array of 1 .. 100 and name it `a`
- create another array with the same values as array `a` and name it `b`
- change the first element of `b` to 0
- print `a` and `b`

"""
