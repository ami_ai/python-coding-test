"""
Task 5
------

- What are abstract classes?
- What are abstractmethod decorator used for in abstract classes?
- Write an abstract class with three empty methods: __init__, set, and get.
    Make set() and get() abstract method

"""
