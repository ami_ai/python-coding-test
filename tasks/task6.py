"""
Task 6
------

- What is threading.Event() used for?
- What are the 3 status of the threading Event and what do they mean?
- Synchronize the execution of the following two functions sensor_x() and sensor_y()
    using appropriate threading events where needed.

"""
import os
import traceback

from apscheduler.schedulers.background import BackgroundScheduler
from datetime import datetime
from random import random
from time import time, sleep
from threading import Thread, Event


def sensor_x():
    return {
        "time": datetime.strftime(datetime.now(), "%Y-%m-%dT%H:%M:%S.%fZ"),
        "x1": str(random()),
    }


def sensor_y():
    return {
        "time": datetime.strftime(datetime.now(), "%Y-%m-%dT%H:%M:%S.%fZ"),
        "y1": str(random()),
        "y2": str(random()),
    }


def log_worker(func, sample_rate, savepath):
    with open(savepath, "w") as f:
        reading = func()
        columns = ",".join(list(reading.keys())) + "\n"
        f.write(columns)

    def internal_func():
        try:
            with open(savepath, "a") as f:
                reading = func()
                vals = ",".join(list(reading.values())) + "\n"
                f.write(vals)
        except:
            error_message = traceback.format_exc()
            print("fatal error:" + error_message)

    scheduler = BackgroundScheduler()
    scheduler.add_job(
        internal_func,
        trigger="interval",
        seconds=float(1 / sample_rate),
        id=func.__name__,
        next_run_time=datetime.now(),
        max_instances=2,
        misfire_grace_time=None,
    )
    scheduler.start()


savedir = "results/"
if not os.path.exists(savedir):
    os.makedirs(savedir)

x_thread = Thread(
    target=log_worker,
    args=[sensor_x, 1, "results/x.txt"],
)
y_thread = Thread(
    target=log_worker,
    args=[sensor_y, 1, "results/y.txt"],
)

x_thread.start()
y_thread.start()

print("=> starting threads ...")
sleep(3)

while True:
    pass
